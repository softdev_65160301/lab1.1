/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.fluke.lab1ver2;

import java.util.Scanner;

/**
 *
 * @author WIN10
 */
public class Lab1ver2 {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char nowPlayer = 'X';
    static int row;
    static int col;
    static String con;
    static boolean play = true;
    public static void main(String[] args) {
        printWelcome();
        while(play){
            while(true){
            printTable();
            printTurn();
            inputRowCol();
            if(isWin()){
                printTable();
                printWinner();
                break;
            }
            if(isDraw()){
                printTable();
                printDraw();
                break;
            }
            switchTurn();
            
        }
        inputContinue();
        if(isContinue()){
            play = true;
            resetTable();
            switchTurn();
        }else{
            break;
        }
        }
        
    }
    
    private static void printWelcome(){
        System.out.println("Welcome to XO game");
    }
    
    private static void printTable() {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    private static void printTurn(){
        System.out.println(nowPlayer + " turn");
    }
    
    private static void inputRowCol(){
        Scanner kb = new Scanner(System.in);
        while(true){
        System.out.print("Please input row,col : ");
        row=kb.nextInt();
        col=kb.nextInt();
        if(table[row-1][col-1]=='-'){
            table[row-1][col-1]=nowPlayer;
            return;
        }
      }
    }

    private static void switchTurn() {
        if(nowPlayer=='X'){
            nowPlayer = 'O';
        }else{
            nowPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if(checkRow()){
            return true;
        }
        if(checkCol()){
            return true;
        }
        if(checkDia1()){
            return true;
        }
        if(checkDia2()){
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for(int i=0;i<3;i++){
            if(table[row-1][i]!= nowPlayer){
                return false;
            }
        }
        return true;
    }

    private static void printWinner() {
        System.out.println(nowPlayer + " win !!!");
    }

    private static boolean checkCol() {
        for(int i=0;i<3;i++){
            if(table[i][col-1]!= nowPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkDia1() {
        for(int i=0;i<3;i++){
            if(table[i][i]!= nowPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkDia2() {
        for(int i=0;i<3;i++){
            if(table[0+i][2-i]!= nowPlayer){
                return false;
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw !!!");
    }

    private static boolean isDraw() {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]=='-'){
                    return false;
                }
            }
        }
        return true;
    }

    private static void inputContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Continue ?(Y?N): ");
        con = sc.next();
    }

    private static boolean isContinue() {
        if(con.equals("Y")){
            return true;
        }else{
            return false;
        }
        
    }

    private static void resetTable() {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                table[i][j] = '-';
            }
            System.out.println("");
        }

    }

    
}
